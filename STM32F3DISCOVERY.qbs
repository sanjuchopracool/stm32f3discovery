import qbs 1.0
import qbs.FileInfo
import qbs.Environment

Product {
    type: ["application", "hex", "bin", "size"]
    Depends { name: "cpp" }
    cpp.executableSuffix: ".out"
    cpp.executablePrefix: ""

    Rule {
        id: hex
        inputs: ["application"]
        prepare: {
            var args = ["-O", "ihex", input.filePath, output.filePath];
            var objcopyPath = input.cpp.objcopyPath
            var cmd = new Command(objcopyPath, args);
            cmd.description = "converting to hex: " + FileInfo.fileName(input.filePath);
            cmd.highlight = "linker";
            return cmd;

        }
        Artifact {
            fileTags: ["hex"]
            filePath: FileInfo.baseName(input.filePath) + ".hex"
        }
    }

    Rule {
        id: bin
        inputs: ["application"]
        prepare: {
            var objcopyPath = input.cpp.objcopyPath
            var args = ["-O", "binary", input.filePath, output.filePath];
            var cmd = new Command(objcopyPath, args);
            cmd.description = "converting to bin: "+ FileInfo.fileName(input.filePath);
            cmd.highlight = "linker";
            return cmd;

        }
        Artifact {
            fileTags: ["bin"]
            filePath: FileInfo.baseName(input.filePath) + ".bin"
        }
    }

    Rule {
        id: size
        inputs: ["application"]
        alwaysRun: true
        prepare: {
            var sizePath = input.cpp.objcopyPath.slice(0, -7) + "size"
            var args = [input.filePath];
            var cmd = new Command(sizePath, args);
            cmd.description = "File size: " + FileInfo.fileName(input.filePath);
            cmd.highlight = "linker";
            return cmd;
        }
        Artifact {
            fileTags: ["size"]
            filePath: undefined
        }
    }

    FileTagger {
        patterns: "*.ld"
        fileTags: ["linkerscript"]
    }

    cpp.cLanguageVersion: "gnu11"
    cpp.cxxLanguageVersion: "c++11"
    cpp.positionIndependentCode: false
    cpp.optimization: "none"
    cpp.debugInformation: true
    cpp.enableExceptions: false
    cpp.enableRtti: false
    cpp.enableReproducibleBuilds: true
    cpp.treatSystemHeadersAsDependencies: true

    cpp.driverFlags:
        [
        "-mcpu=cortex-m4",
        "-mthumb",
        "-mfloat-abi=hard",
        "-mfpu=fpv4-sp-d16",
        "--specs=nano.specs",
        "-ffunction-sections",
        "-fdata-sections",
        "-Wall",
        "-fstack-usage",
    ]

    cpp.cxxFlags: [
        "-fno-threadsafe-statics",
        "-fno-use-cxa-atexit"
    ]

    cpp.linkerFlags: [
        "--gc-sections",
        "-u,Reset_Handler"
    ]


    cpp.defines: [
        "USE_HAL_DRIVER",
        "USE_STM32F3_DISCO",
        "STM32F303xC"
    ]


    property  string cubePackagePath: Environment.getEnv("HOME") + "/STM32Cube/Repository/STM32Cube_FW_F3_V1.10.0/"
    property  string firmwareDriverPath: cubePackagePath + "Drivers/"
    property string  driverPath: firmwareDriverPath + "STM32F3xx_HAL_Driver/"
    property string  driverIncludePath: driverPath + "Inc/"
    property string  driverLegacyIncludePath: driverIncludePath + "Legacy/"
    property string  driverSourcesPath: driverPath + "Src/"

    property string bspDriverPath: firmwareDriverPath + "BSP/STM32F3-Discovery/"
    property string componentsDriverPath: firmwareDriverPath + "BSP/Components/"

    property  string cmsisPath: firmwareDriverPath + "CMSIS/"
    property string  cmsisIncludePath: cmsisPath + "Include/"
    property string  cmsisDeviceIncludePath: cmsisPath + "Device/ST/STM32F3xx/Include"

    property string middlewaresPath: cubePackagePath + "Middlewares/"
    property string freeRtosPath : middlewaresPath + "Third_Party/FreeRTOS/Source/"
    property string freeRtosIncludePath : freeRtosPath + "include/"
    property string freeRtosPortablePath: freeRtosPath + "portable/GCC/ARM_CM4F"
    property string freeRtosCmsisIncludePath : freeRtosPath + "CMSIS_RTOS/"

    property string middlewareSTPath: middlewaresPath + "ST/"
    property string stUSBHostLibPath: middlewareSTPath + "STM32_USB_Host_Library/"
    property string usbHostCoreIncludePath: stUSBHostLibPath + "Core/Inc/"
    property string usbHostCDCIncludePath: stUSBHostLibPath + "Class/CDC/Inc/"
    property  string projectSourcesPath: "Src/"
    cpp.includePaths:
        [
        ".",
        "./Inc",
        driverIncludePath,
        driverLegacyIncludePath,
        cmsisIncludePath,
        cmsisDeviceIncludePath,
        freeRtosPortablePath,
        freeRtosIncludePath,
        freeRtosCmsisIncludePath,
        bspDriverPath,
        componentsDriverPath + "l3gd20/",
        componentsDriverPath + "lsm303dlhc/",
    ]

    files: {
        var projectFiles = [
                    "SW4STM32/STM32F3-Discovery/STM32F303VCTx_FLASH.ld",
                    "SW4STM32/startup_stm32f303xc.S",
                    "DEBUGGING.txt"
                ];

        var headers = [
                    "FreeRTOSConfig.h",
                    "main.h",
                    "stm32f3xx_hal_conf.h",
                    "stm32f3xx_it.h",
                    "mems.h"
                ]

        var sources = [
                    "main.c",
//                    "stm32f3xx_hal_timebase_tim.c",
                    "stm32f3xx_it.c",
                    "system_stm32f3xx.c",
                    "mems.c"
                ]
        var librarySources = [
                    "stm32f3xx_hal_cortex.c",
                    "stm32f3xx_hal_dma.c",
                    "stm32f3xx_hal_gpio.c",
                    "stm32f3xx_hal_i2c_ex.c",
                    "stm32f3xx_hal_i2c.c",
                    "stm32f3xx_hal_rcc.c",
                    "stm32f3xx_hal_rcc_ex.c",
                    "stm32f3xx_hal_rtc.c",
                    "stm32f3xx_hal_rtc_ex.c",
                    "stm32f3xx_hal_spi.c",
                    "stm32f3xx_hal.c"
                ]

        var freeRtosSources = [
                    "CMSIS_RTOS/cmsis_os.c",
                    "portable/GCC/ARM_CM4F/port.c",
                    "portable/MemMang/heap_4.c",
                    "list.c",
                    "tasks.c",
                    "queue.c",
                    "timers.c",
                    "croutine.c",
                    "event_groups.c",
//                    "stream_buffer.c",
                    "readme.txt",
                ]

        var bspSources =
                [
                    "stm32f3_discovery_accelerometer.c",
                    "stm32f3_discovery_accelerometer.h",
                    "stm32f3_discovery.c",
                    "stm32f3_discovery_gyroscope.c",
                    "stm32f3_discovery_gyroscope.h",
                    "stm32f3_discovery.h"
                ]

        var componentsSources =
                [
                    "l3gd20/l3gd20.c",
                    "lsm303dlhc/lsm303dlhc.c"
                ]

        for(var i=0; i<sources.length; i++){
            projectFiles.push( projectSourcesPath + sources[i]);
        }

        for(var i=0; i<librarySources.length; i++){
            projectFiles.push( driverSourcesPath + librarySources[i]);
        }

        for(var i=0; i<freeRtosSources.length; i++){
            projectFiles.push( freeRtosPath + freeRtosSources[i]);
        }

        for(var i=0; i<headers.length; i++){
            projectFiles.push( "./Inc/" + headers[i]);
        }

        for(var i=0; i<bspSources.length; i++){
            projectFiles.push( bspDriverPath + bspSources[i]);
        }

        for(var i=0; i<componentsSources.length; i++){
            projectFiles.push( componentsDriverPath + componentsSources[i]);
        }

        return projectFiles;
    }

    cpp.staticLibraries:
        [
        "gcc",
        "c_nano",
        "m",
        "nosys",
    ]
}
